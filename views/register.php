<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container toparea">
		<div id="edd_checkout_wrap" class="col-md-8 col-md-offset-2">
			<form>
				<div class="form-group">
			    	<label for="firstname">Firstname</label>
			    	<input type="text" class="form-control" id="firstname" aria-describedby="emailHelp" placeholder="Enter firstname">
			  	</div>
			  	<div class="form-group">
			    	<label for="lastname">Lastname</label>
			    	<input type="text" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter lastname">
			  	</div>
			  	<div class="form-group">
			    	<label for="middlename">Middlename</label>
			    	<input type="text" class="form-control" id="middlename" aria-describedby="emailHelp" placeholder="Enter middlename">
			  	</div>
			  	<hr>
			  	<div class="form-group">
			    	<label for="address">Address</label>
			    	<input type="text" class="form-control" id="address" aria-describedby="emailHelp" placeholder="Enter address">
			  	</div>
			  	<div class="form-group">
				    <label for="sex">Sex</label>
				    <select class="form-control" id="sex">
				      <option value="Male">Male</option>
				      <option value="Female">Female</option>
				    </select>
 				</div>
			  	<div class="form-group">
			    	<label for="birthday">Birthday</label>
			    	<input type="date" class="form-control" id="birthday" aria-describedby="emailHelp" placeholder="Enter birthday">
			  	</div>
			  	<hr>
			  	<div class="form-group">
			    	<label for="mobilenumber">Mobile Number</label>
			    	<input type="text" class="form-control" id="mobilenumber" aria-describedby="emailHelp" placeholder="Enter mobilenumber">
			  	</div>
			  	<div class="form-group">
			    	<label for="exampleInputEmail1">Email address</label>
			    	<input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
			  	</div>
			  	<div class="form-group">
			    	<label for="exampleInputPassword1">Password</label>
			    	<input type="password" class="form-control" id="password" placeholder="Password">
			  	</div>
			  	<hr>
			  	<button type="button" class="btn btn-primary register-btn">Submit</button>
			</form>
		</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>

<script>
	$('.register-btn').click(function() {
		let firstname = $('#firstname').val()
		let lastname = $('#lastname').val()
		let middlename = $('#middlename').val()
		let address = $('#address').val()
		let mobilenumber = $('#mobilenumber').val()
		let sex = $('#sex').val()
		let birthday = $('#birthday').val()
		let email = $('#email').val()
		let password = $('#password').val()

		if (!firstname || !lastname || !address || !mobilenumber || !sex || !birthday || !email || !password) {
			alert('Please enter your data');
			return;
		}

	    $.ajax({
	    	type: 'POST',
	    	url: '/api-register',
	    	data: {
	    		firstname: firstname,
	    		lastname: lastname,
	    		middlename: middlename,
	    		address: address,
	    		mobilenumber: mobilenumber,
	    		sex: sex,
	    		birthday: birthday,
	    		email: email,
	    		password: password
	    	},
	    	success: function (res) {
	    		alert('Registered Successfully!');
	    		window.location.replace('/login');
	    	},
	    	error: function (res) {
	    		console.log(res)
	    	}
	    });
	});
</script>