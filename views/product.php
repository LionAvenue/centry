<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container toparea">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems"><?php echo $vendorTicket['name'] ?></h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="productbox">
					<?php $image =  $vendorTicket['image'] &&  $vendorTicket['image'] != 'null' ? $vendorTicket['image'] : 'notfound.jpg'; ?>
					<img src="../public/images/vendor/<?php echo $image;?>" alt="" style="height: 481px; width: 100%; object-fit: cover;">
					<div class="clearfix">
					</div>
					<div class="product-details text-center" style='text-transform:uppercase'>
						<p>
							<?php echo $vendorTicket['description'] ?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<a href="#" class="btn btn-buynow">&#8369;<?php echo $vendorTicket['price'] ?> - Purchase</a>
				<hr>
				<div class="row" style="margin-bottom: 15px;">
			        <div class="col-xs-12">
			            <div class="input-group">
		                    <span class="input-group-btn">
		                        <button type="button" class="quantity-left-minus btn btn-default btn-number"  data-type="minus" data-field="">
		                          <span class="glyphicon glyphicon-minus"></span>
		                        </button>
		                    </span>
		                    <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
		                    <span class="input-group-btn">
		                        <button type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="">
		                            <span class="glyphicon glyphicon-plus"></span>
		                        </button>
		                    </span>
		                </div>
			        </div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						 <div class="form-group">
						    <input 
						    	type="date" 
						    	class="form-control" 
						    	id="date" 
						    	aria-describedby="date" 
						    	placeholder="Date"
						    	value="<?php echo date('Y-m-d') ?>"
						    	min="<?php echo date('Y-m-d')?>"
						    	>
						  </div>
					</div>
				</div>
					<div class="row">
					<div class="col-xs-12">
						 <div class="form-group">
						    <input 
						    	type="time" 
						    	class="form-control" 
						    	id="time"
						    	value="<?php echo date('H:i')?>"
						    	aria-describedby="time" 
						    	placeholder="Time"
						    	>
						  </div>
					</div>
				</div>
				<div class="properties-box">
					<ul class="unstyle">
						<li style="text-transform:uppercase"><b class="propertyname"><?php echo $vendor['name'] ?></b></li>
						<li><b class="propertyname"><?php echo $vendor['address'] ?></b></li>
						<li><b class="propertyname"><?php echo $vendor['email'] ?></b></li>
						<li><b class="propertyname"><?php echo $vendor['phonenumber'] ?></b></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	let user = '<?php echo !$user ? 0 : 1   ?>';
	let vendor = <?php echo json_encode($vendor); ?>;
	let vendorSched = vendor.vendor_schedule;
	let weekdays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

	var quantitiy = 1;

	$('.btn-buynow').click(function() {
		if (!user) {
			alert('Please login your account!');
		} else {
			let date = $('#date').val();
			let time = $('#time').val();
			let today = moment();

			if (!date) {
				alert('Please choice your date!');
				return;
			}

			if (!time) {
				alert('Please choice your time!');
				return;
			}

			let selectedDay = weekdays[moment(date).day()];

			$hasDay = vendorSched.some(a => a.day === selectedDay);

			if (!$hasDay) {
				alert('Store is close on that day!');
				return;
			}

			let weekday = weekdays[moment(date).day()];
			let schedule = vendorSched.find(a => a.day == weekday);

			if (!schedule) {
				alert('Vendor is closed on that date!');
				return;
			}

			var format = 'HH:mm:ss'

			var selectedTime = moment(time, format),
			  beforeTime = moment(schedule.start_time, format),
			  afterTime = moment(schedule.end_time, format)

			if (selectedTime.isBetween(beforeTime, afterTime) || selectedTime.isSame(beforeTime) || selectedTime.isSame(afterTime)) {
				let finalQuantity = parseInt($('#quantity').val());
				let queryString = window.location.search;
				let urlParams = new URLSearchParams(queryString);
				window.location.href = `/checkout${queryString}&quantity=${finalQuantity}&dateStart=${date}&timeStart=${time}`;

				return;
			} else {
				alert('Time is not available');
				return;
			}
		}
	});

   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
            
            $('#quantity').val(quantity + 1);

          
            // Increment
        
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
      
            // Increment
            if(quantity > 1){
             $('#quantity').val(quantity - 1);
            }
    });
    
});
</script>