<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container">
		<div class="container-toparea">
			<div class="row">
				<div class="col-md-4">
					<h1>My Profile</h1>
					<hr>
					<div class ="container-personal">
						<h2>Personal</h2>
						<h5 class="display-form">Name: <?php echo ucfirst($user['firstname']).' '.$user['middlename'].' '.$user['lastname']; ?></h5>

						<div class="form-group form-edit" style="display: none">
					    	<label for="firstname">Firstname</label>
					    	<input type="text" class="form-control" id="firstname" value="<?php echo $user['firstname'] ?>">
					  	</div>

						<div class="form-group form-edit" style="display: none">
					    	<label for="middlename">Middlename</label>
					    	<input type="text" class="form-control" id="middlename" value="<?php echo $user['middlename'] ?>">
					  	</div>

					  	<div class="form-group form-edit" style="display: none">
					    	<label for="lastname">Lastname</label>
					    	<input type="text" class="form-control" id="lastname" value="<?php echo $user['lastname'] ?>">
					  	</div>

						<h5 class="display-form">Sex: <?php echo ucfirst($user['sex']); ?></h5>

						<div class="form-group form-edit" style="display: none">
                            <label>Sex</label>
                            <select class="form-control" id="sex">
                                <option value="male" <?php echo $user['sex'] == 'male' ? 'selected' : ''?>>Male</option>
                                <option value="female" <?php echo $user['sex'] == 'female' ? 'selected' : ''?>>Female</option>
                            </select>
                        </div>

						<h5 class="display-form">Birthdate: <?php echo ucfirst($user['birthday']); ?></h5>
						<div class="form-group form-edit" style="display: none">
                            <label>Birthday</label>
                            <input type="date" class="form-control" id="birthday" value="<?php echo $user['birthday']?>">
                        </div>
					</div>
					<hr>

					<div class ="container-personal">
						<h2>Contact</h2>
						<h5 class="display-form">Email: <?php echo ucfirst($user['email']); ?></h5>
					   	<div class="form-group form-edit" style="display: none">
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" value="<?php echo $user['email']?>">
                        </div>


						<h5 class="display-form">Mobile #: <?php echo ucfirst($user['mobilenumber']); ?></h5>
						<div class="form-group form-edit" style="display: none">
                            <label>Mobile Number</label>
                            <input type="text" class="form-control" id="mobilenumber" value="<?php echo $user['mobilenumber']?>">
                        </div>

						<h5 class="display-form">Address: <?php echo ucfirst($user['address']); ?></h5>
						<div class="form-group form-edit" style="display: none">
                            <label>Address</label>
                            <input type="text" class="form-control" id="address" value="<?php echo $user['address']?>">
                        </div>

                        <div class="form-group form-edit" style="display: none">
                            <label>password</label>
                            <div>
                                <input type="text" class="form-control" id="password" value="">
                            </div>
                        </div>
						<hr>
						<button class="btn btn-primary edit-btn">Edit</button>
						<button class="btn btn-success save-btn" style="display: none">Save</button>
						<button class="btn btn-primary logout-btn">Logout</button>
					</div>

					<?php if ($user['role'] >= 2) { ?>
						<hr>
						<a href="/dashboard" class="btn btn-block btn-warning">Admin Dashboard</a>
					<?php }	?>
				</div>
				<div class="col-md-8">
					<h5>Travel history</h5>
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">vendor</th>
					      <th scope="col">ticket</th>
					      <th scope="col">quantity</th>
					      <th scope="col">price</th>
					      <th scope="col">date</th>
					      <th scope="col">status</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach ($reservationList as $key => $value) { ?>
					  		<tr>
						      <th scope="row"><?php echo $key; ?></th>
						      <td><?php echo $value['vendor']['name'] ?></td>
						      <td><?php echo $value['ticket']['name'] ?></td>
						      <td><?php echo $value['quantity'] ?></td>
						      <td><?php echo $value['total_price'] ?> php</td>
						      <td><?php echo $value['date_start'] ?></td>
						      <td>
						      	<?php if ($value['is_approved'] == 0) { ?>
						      		<button class="btn btn-warning" disabled="true">Pending</button>
						      	<?php } elseif ($value['is_approved'] == 1) { ?>
						      		<button class="btn btn-success" disabled="true">Approved</button>
						      	<?php } else { ?>
						      		<button class="btn btn-danger" disabled="true">Disapproved</button>
						      	<?php } ?>
						      </td>
						    </tr>
					  	<?php } ?>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	<!-- <div class="container-rightarea">
			<h1>Travel History</h1>
	</div> -->
</section>

<?php require('layout/footer.php'); ?>

<script>
	$('.logout-btn').click(function() {
		$.ajax({
	      	type: 'POST',
	      	url: '/api-logout',
	      	dataType: 'JSON',
		    success: function (data) {
		  		if (data.status == 'ok') {
		  			window.location.replace('/');
		  		} else {
		  			alert(data.message);
		  		}      
		    },
	      	error: function (data) {
	        	console.log(data)
	     	}
	    });
	});

	$('.edit-btn').click(function() {
		$('.form-edit').show();
		$('.display-form').hide();
		$(this).hide();
		$('.save-btn').show();
	});

	$('.save-btn').click(function() {
		let that = this;
		let id = '<?php echo $user['id'] ?>';
        let firstname = $('#firstname').val()
        let lastname = $('#lastname').val()
        let middlename = $('#middlename').val()
        let address = $('#address').val()
        let mobilenumber = $('#mobilenumber').val()
        let sex = $('#sex').val()
        let birthday = $('#birthday').val()
        let email = $('#email').val()
        let password = $('#password').val()
        let role = '<?php echo $user['role'] ?>';

        if (!firstname || !lastname || !address || !mobilenumber || !sex || !birthday || !email) {
			alert('Please enter your data');
			return;
		}

        $.ajax({
            type: 'POST',
            url: '/api-update',
            data: {
                id: id,
                firstname: firstname,
                lastname: lastname,
                middlename: middlename,
                address: address,
                mobilenumber: mobilenumber,
                sex: sex,
                birthday: birthday,
                email: email,
                password: password,
                role: role
            },
            success: function (res) {
                alert('Updated Successfully!');
                $(that).hide();
				$('.edit-btn').show();
				$('.form-edit').hide();
				$('.display-form').show();
				location.reload();
            },
            error: function (res) {
                console.log(res)
            }
        });
	});
</script>