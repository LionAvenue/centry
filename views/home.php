<?php require('layout/header.php'); ?>

<!-- STEPS =============================-->
<div class="item content">
	<div class="container toparea">
		<div class="row text-center">
			<div class="col-md-4">
				<div class="col editContent">
					<span class="numberstep"><i class="fa fa-shopping-cart"></i></span>
					<h3 class="numbertext">Choose Partnered Venues</h3>
					<p>
						 All venues partnered with us are verified and qualified to ensure a safe and fulfilling experience. We only partner with destinations that are of the highest quality the Philippines has to offer. <br> Check the <a href="/shop">Venues</a> to see the list of spectacles for you to visit.
					</p>
				</div>
				<!-- /.col-md-4 -->
			</div>
			<!-- /.col-md-4 col -->
			<div class="col-md-4 editContent">
				<div class="col">
					<span class="numberstep"><i class="fa fa-gift"></i></span>
					<h3 class="numbertext">Pay and Book Online</h3>
					<p>
						 Payment and booking online couldn't have been any easier! Our services provide an updated list of every venue's prices, events, offers and discounts to help you get the most value with your experience. 
					</p>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.col-md-4 col -->
			<div class="col-md-4 editContent">
				<div class="col">
					<span class="numberstep"><i class="fa fa-download"></i></span>
					<h3 class="numbertext">Download Transactions</h3>
					<p>
						 Your transactions can be downloaded through the website so that you can have a digital copy of your ticket. You may just simply show your receipt to any of our partnered venues to verify your payment.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
	
	
<!-- LATEST ITEMS =============================-->
<section class="item content">
	<div class="container">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems">POPULAR VENUES</h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="productbox">
					<div class="fadeshop">
						<div class="captionshop text-center" style="display: none;">
							<h3>Manila Ocean Park</h3>
							<p>
								 Is the first world-class marine theme park in the Philippines.
							</p>
							<p>
								<a href="/vendor" class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
							</p>
						</div>
						<span class="maxproduct"><img class="vendor-thumbnail" src="../public/client/images/vendor2.jpg" alt=""></span>
					</div>
					<div class="product-details">
						<a href="#">
						<h1>Manila Ocean Park</h1> <p> Metro Manila </p>
						</a>
					</div>
				</div>
			</div>
			<!-- /.productbox -->
			<div class="col-md-4">
				<div class="productbox">
					<div class="fadeshop">
						<div class="captionshop text-center" style="display: none;">
							<h3> Enchanted Kingdom</h3>
							<p>
								 Is the only world-class theme park in the Philippines.
							</p>
							<p>
								<a href="/vendor" class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
							</p>
						</div>
						<span class="maxproduct"><img class="vendor-thumbnail" src="../public/client/images/vendor1.jpg" alt=""></span>
					</div>
					<div class="product-details">
						<h1>  Enchanted Kingdom </h1> <p>Sta. Rosa, Laguna</p>
						</a>
					</div>
				</div>
			</div>
			<!-- /.productbox -->
			<div class="col-md-4">
				<div class="productbox">
					<div class="fadeshop">
						<div class="captionshop text-center" style="display: none;">
						<h3>Splash Island</h3>
							<p>
								 Is a lively waterpark with large slides, pools & a lazy river, plus a food court & gift shop.
							</p>
							<p>
								<a href="/vendor" class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
							</p>
						</div>
						<span class="maxproduct"><img class ="vendor-thumbnail" src="../public/client/images/vendor3.jpeg" alt=""></span>
					</div>
					<div class="product-details">
						<a href="#">
						<h1>Splash Island</h1> <p>Biñan, Laguna</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>


<!-- BUTTON =============================-->
<div class="item content">
	<div class="container text-center">
		<a href="/vendor" class="homebrowseitems">Browse All Venues
		<div class="homebrowseitemsicon">
			<i class="fa fa-star fa-spin"></i>
		</div>
		</a>
	</div>
</div>
<br/>


<!-- AREA =============================-->
<div class="item content">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<i class="fa fa-question infoareaicon"></i>
				<div class="infoareawrap">
					<h1 class="text-center subtitle">General Questions</h1>
					<p>
						 Not sure about the prices or anything with our services? Make sure to direct any inquiries using our contact information at <a href="/contact">Contacts</a>. 
					</p>
				</div>
			</div>
			<!-- /.col-md-4 col -->
			<div class="col-md-4">
				<i class="fa fa-certificate infoareaicon"></i>
				<div class="infoareawrap">
					<h1 class="text-center subtitle">Want to Partner with us?</h1>
					<p>
						 If you are a vendor, programmer, or a tourist agency, and wish to partner with Centry, feel free to use the information provided in the <a href="/contact">Contacts</a> and come talk to us~
					</p>
				</div>
			</div>
			<!-- /.col-md-4 col -->
			<div class="col-md-4">
				<i class="fa fa-info-circle infoareaicon"></i>
				<div class="infoareawrap">
					<h1 class="text-center subtitle">Learn More About Us</h1>
					<p>
						 If you wish to learn more about the conceptualization and idea behind Centry, go to <a href="/contact">Contact</a> and learn more about us!
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- TESTIMONIAL =============================-->
<div class="item content">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="slide-text">
				<div>
					<h2><span class="uppercase">Thank You for Checking us Out!</span></h2>
					<h1 class="home-foot">CENTRY</h1>
					<p>
						 Centry only aims to be the central repository for travel destinations in the Philippines. Our goal is to make travelling and booking tickets online easy, accessible, and convenient for our users. 
					</p>
					<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require('layout/footer.php'); ?>