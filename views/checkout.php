<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container toparea">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems">SUMMARIES</h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<div id="edd_checkout_wrap" class="col-md-8 col-md-offset-2">
			<form id="edd_checkout_cart_form" method="post">
				<div id="edd_checkout_cart_wrap">
					<table id="edd_checkout_cart" class="ajaxed">
					<thead>
					<tr class="edd_cart_header_row">
						<th class="edd_cart_item_name">
							 Item Name
						</th>
						<th class="edd_cart_item_price">
							 Item Price
						</th>
						<th class="edd_cart_actions">
							 Quantity
						</th>
						<th class="edd_cart_actions">
							 Date
						</th>
						<th class="edd_cart_actions">
							 Time
						</th>
					</tr>
					</thead>
					<tbody>
					<tr class="edd_cart_item" id="edd_cart_item_0_25" data-download-id="25">
						<td class="edd_cart_item_name">
							<div class="edd_cart_item_image">
								<?php $image =  $vendorTicket['image'] && $vendorTicket['image'] != 'null' ? $vendorTicket['image'] : 'notfound.jpg'; ?>
								<img width="25" height="25" src="public/images/vendor/<?php echo $image?>" alt="">
							</div>
							<span class="edd_checkout_cart_item_title"><?php echo $vendorTicket['name'] ?></span>
						</td>
						<td class="edd_cart_item_price">
							<?php echo $vendorTicket['price'] ?>
						</td>
						<td class="edd_cart_actions">
							<?php $quantity = $_GET['quantity']; echo $quantity ?>
						</td>
						<td class="edd_cart_actions">
							<?php $dateStart = $_GET['dateStart']; echo $dateStart ?>
						</td>
						<td class="edd_cart_actions">
							<?php $timeStart = $_GET['timeStart']; echo $timeStart ?>
						</td>
					</tr>
					</tbody>
					<tfoot>
					<tr class="edd_cart_footer_row">
						<th colspan="5" class="edd_cart_total">
							<?php 
								$totalPrice = $_GET['quantity'] * $vendorTicket['price'];
							?>
							Total: <span class="edd_cart_amount" data-subtotal="11.99" data-total="11.99"><?php echo $totalPrice ?> Php</span>
						</th>
					</tr>
					</tfoot>
					</table>
				</div>
			</form>
			<div id="edd_checkout_form_wrap" class="edd_clearfix">
				<form id="edd_purchase_form" class="edd_form" action="#" method="POST">
					<fieldset id="edd_checkout_user_info">
						<legend>Personal Info</legend>
						<p id="edd-email-wrap">
							<label class="edd-label" for="edd-email">
							Email Address <span class="edd-required-indicator">*</span></label>
							<input class="edd-input required" type="email" name="edd_email" placeholder="Email address" id="edd-email" value="<?php echo $user['email']?>" disabled>
						</p>
						<p id="edd-first-name-wrap">
							<label class="edd-label" for="edd-first">
							First Name <span class="edd-required-indicator">*</span>
							</label>
							<input class="edd-input required" type="text" name="edd_first" placeholder="First name" id="edd-first" value="<?php echo $user['firstname']?>" disabled>
						</p>
						<p id="edd-last-name-wrap">
							<label class="edd-label" for="edd-last">
							Last Name </label>
							<input class="edd-input" type="text" name="edd_last" id="edd-last" placeholder="Last name" value="<?php echo $user['lastname']?>" disabled>
						</p>
					</fieldset>
					<fieldset id="edd_purchase_submit">
						<p id="edd_final_total_wrap">
							<strong>Amount to Pay:</strong>
							<span class="edd_cart_amount" data-subtotal="11.99" data-total="11.99"><?php echo $totalPrice ?> Php</span>
						</p>
						<input type="hidden" name="edd_action" value="purchase">
						<input type="hidden" name="edd-gateway" value="manual">
						<input type="button" class="edd-submit button" id="edd-purchase-button" name="edd-purchase" value="Purchase">
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>
<script type="text/javascript">
	$('#edd-purchase-button').click(function() {
		let vendor = <?php echo json_encode($vendor); ?>;
		let ticket = <?php echo json_encode($vendorTicket); ?>;
		let user = <?php echo json_encode($user); ?>;
		let quantity = <?php echo $quantity; ?>;
		let totalPrice = <?php echo $totalPrice; ?>;
		let dateStart = "<?php echo $dateStart; ?>";
		let timeStart = "<?php echo $timeStart; ?>";
		let today = moment().format('YYYY-MM-DD');

		$.ajax({
	      	type: 'POST',
	      	url: '/api-reservation',
	      	data: {
	      		vendor_id: vendor.id, 
	      		ticket_id: ticket.id,
	      		user_id: user.id,
	      		quantity,
	      		totalPrice,
	      		dateStart,
	      		timeStart,
	      		today
	      	},
	      	dataType: 'JSON',
		    success: function (data) {
		  		if (data.status == 'ok') {
		  			window.location.replace('/congratulations');
		  		} else {
		  			alert("Something wen't wrong!");
		  		} 
		    },
	      	error: function (data) {
	        	console.log(data)
	     	}
	    });
	});
</script>