<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container toparea">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems">Tickets</h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<?php 
			$ticketList = $vendorTicketList['tickets'];

			if(count($ticketList)) { ?>
		<div class="row">
			<?php foreach($ticketList as $key => $value) { 
				$image =  $value['image'] && $value['image'] != 'null' ? $value['image'] : 'notfound.jpg';
				?>
				<div class="col-md-3">
					<div class="productbox">
						<div class="fadeshop">
							<div class="captionshop text-center" style="display: none;">
								<h3> <?php echo $value['name']; ?></h3>
								<p>
									 <?php echo $value['description']; ?>
								</p>
								<p>
									<a href="/product?vendor=<?php echo $vendorTicketList['slug']?>&product=<?php echo $value['slug']?>" class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
								</p>
							</div>
							<span class="maxproduct"><img class="vendor-thumbnail" src="../public/images/vendor/<?php echo $image;?>" alt=""></span>
						</div>
						<div class="product-details">
							<h1>  <?php echo $value['name']; ?> </h1> <p> </p>
							</a>
							<span class="price">
								<span class="edd_price">&#8369;<?php echo $value['price']; ?></span>
							</span>
						</div>
					</div>
				</div>
			<?php }?>
		</div>
		<?php } else { ?>
		<div class="row">
			<div class="col-12 text-center">
				<h3>NO TICKETS AVAILABLE!</h3>
			</div>
		</div>
		<?php } ?>
	</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>