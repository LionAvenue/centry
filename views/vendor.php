<?php require('layout/header.php'); ?>
<section class="item content">
	<div class="container toparea">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems">OUR VENDORS</h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<div class="row">
			<?php foreach($vendorList as $index => $value) { 
				$image =  $value['image'] ? $value['image'] : 'notfound.jpg';
				?>
				<div class="col-md-4">
					<div class="productbox">
						<div class="fadeshop">
							<div class="captionshop text-center" style="display: none;">
								<h3><?php echo $value['name'] ?></h3>
								<p>
									 <?php echo $value['description'] ?>
								</p>
								<p>
									<a href="/shop?vendor=<?php echo $value['slug'] ?>" class="learn-more detailslearn"><i class="fa fa-link"></i> View Tickets</a>
								</p>
							</div>
							<span class="maxproduct"><img class="vendor-thumbnail" src="../public/images/vendor/<?php echo $image;?>" alt=""></span>
						</div>
						<div class="product-details">
							<h1>  <?php echo $value['name'] ?> </h1> <p><?php echo $value['address'] ?></p>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>