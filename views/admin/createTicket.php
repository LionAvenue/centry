<?php require('layout/header.php'); ?>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="wrapper">
    <?php require('layout/navbar.php'); ?>
    <?php require('layout/sidebar.php'); ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php require('layout/small-navbar.php'); ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Create New ticket</h3>
                        <br>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-12">Select Vendor</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="vendorSelect" disabled>
                                        <?php foreach($vendorList as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>" <?php echo $value['id'] == $user['vendor_id'] ? 'selected' : '' ?>>
                                                <?php echo $value['name'] ?>    
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" id="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Price</label>
                                <div class="col-md-12">
                                    <input type="number" class="form-control" id="price" step=0.01>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control-file" id="image">
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-info create-ticket-btn">submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('layout/footer.php'); ?>
<script>
    $('.create-ticket-btn').click(function() {
        let name = $('#name').val();
        let price = $('#price').val();
        let description = $('#description').val();
        let image = null;
        let vendorId = $("#vendorSelect").val();
        let slug = convertToSlug(name);

        if (!name || !price || !description || !vendorId || !slug) {
            alert('Please enter your data');
            return;
        }

         if ($("#image").val()) {
            image = $('#image').prop('files')[0];
        } else {
            image = null;
        }

        let form_data = new FormData()
        form_data.append('name', name)
        form_data.append('description', description)
        form_data.append('price', price)
        form_data.append('image', image)
        form_data.append('vendorId', vendorId)
        form_data.append('slug', slug)

        $.ajax({
            type: 'POST',
            url: '/api-create-ticket',
            async: false,
            contentType: false,
            cache: false,
            processData:false,
            data: form_data,
            dataType: 'JSON',
            success: function (res) {
                alert('Registered Successfully!');

                $('#name').val('');
                $('#price').val('');
                $('#description').val('');
            },
            error: function (res) {
                console.log(res)
            }
        });
    });

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
</script>
