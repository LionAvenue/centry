<?php require('layout/header.php'); ?>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

    <div id="wrapper">
        <?php require('layout/navbar.php'); ?>
        <?php require('layout/sidebar.php'); ?>
        <div id="page-wrapper-admin">
            <div class="container-fluid">
                <?php require('layout/small-navbar.php'); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h1 class="box-title">Centry Dashboard</h1>
                            <div class="item-content-admin-dash">
                                    <div class="container toparea">
                                            <?php if ($user['role'] == 3) { ?>
                                                <div class="col-md-3 editContent-admin">
                                                    <h1 class="numbertext">
                                                    Users</h1>
                                                    <p>
                                                         <h2> <a href="/create-user">Create Users</a> </h2>
                                                         <p> Create new accounts for new users and admin accounts to moderate the page.</p>
                                                         <h2> <a href="/user-list"> User List</a> </h2>
                                                         <p> Browse or Edit all the registered accounts in the page database.</p>
                                                    </p>
                                                    <hr>
                                                </div>
                                            <?php } ?>
                                            <?php if ($user['role'] == 3) { ?>
                                            <div class="col-md-3 editContent-admin">
                                                <div class="col">
                                                     <h1 class="numbertext">
                                                    Vendors</h1>
                                                    <p>
                                                         <h2> <a href="/create-vendor">Create Vendors</a> </h2>
                                                         <p> Create new Vendors that would provide new tickets and orders.</p>
                                                         <h2> <a href="/vendor-list"> Vendor List</a> </h2>
                                                         <p> Browse or Edit all the registered vendors in the page database.</p>
                                                    </p>
                                                    <hr>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="col-md-3 editContent-admin">
                                                <div class="col">
                                                     <h1 class="numbertext">
                                                   Tickets</h1>
                                                    <p>
                                                         <h2> <a href="/create-user">Create Tickets</a> </h2>
                                                         <p> Create new tickets for the vendors to offer to users.</p>
                                                         <h2> <a href="/user-list"> Tickets List</a> </h2>
                                                         <p> Browse or edit all the tickets available for the vendors in the database.</p>
                                                    </p>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="col-md-3 editContent-admin">
                                                <div class="col">
                                                     <h1 class="numbertext">
                                                    Booking</h1>
                                                    <p>
                                                         <h2> <a href="/calendar">Calendar</a> </h2>
                                                         <p> Check all the current registered and ongoing tickets in the calendar.</p>
                                                         <hr>                    
                                                    </p>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require('layout/footer.php'); ?>
