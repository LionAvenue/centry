<?php require('layout/header.php'); ?>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="wrapper">
    <?php require('layout/navbar.php'); ?>
    <?php require('layout/sidebar.php'); ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php require('layout/small-navbar.php'); ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Create New Vendor</h3>
                        <br>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Address</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="address">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Phone Number</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="phonenumber">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" id="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control-file" id="image">
                                </div>
                            </div>
                            <hr>
                            <h3 class="box-title m-b-0">Schedule</h3>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="sunday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Sunday</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="sunday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="sunday-end" value="17:00">
                                    </div>
                                </div>       
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="monday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Monday</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="monday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="monday-end" value="17:00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="tuesday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Tuesday</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="tuesday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="tuesday-end" value="17:00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="wednesday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Wednesday</label>
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="wednesday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="wednesday-end" value="17:00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="thursday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Thursday</label>
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="thursday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="thursday-end" value="17:00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="friday"checked>
                                        <label class="form-check-label" for="exampleCheck1">Friday</label>
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="friday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="friday-end" value="17:00">
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-sm-5"> 
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input availableDay" value="saturday" checked>
                                        <label class="form-check-label" for="exampleCheck1">Saturday</label>
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Start Time</label>
                                        <input type="time" class="form-control" id="saturday-start" value="08:00">
                                    </div>
                                    <div class="form-group">
                                        <label for="">End Time</label>
                                        <input type="time" class="form-control" id="saturday-end" value="17:00">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-info create-vendor-btn">submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('layout/footer.php'); ?>
<script>
    $('.create-vendor-btn').click(function() {
        let name = $('#name').val();
        let address = $('#address').val();
        let email = $('#email').val();
        let phonenumber = $('#phonenumber').val();
        let description = $('#description').val();
        let slug = convertToSlug(name);
        let sched = {};
        let image = $('#image').prop('files')[0]

        if (!name || !address || !email || !phonenumber || !description || !slug || !sched) {
            alert('Please enter your data');
            return;
        }

        let checkedVals = $('.availableDay:checkbox:checked').map(function() {
            return this.value;
        }).get();

        for (var i = checkedVals.length - 1; i >= 0; i--) {
            let day = checkedVals[i];
            let startTime = $(`#${checkedVals[i]}-start`).val();
            let endTime = $(`#${checkedVals[i]}-end`).val();

             sched[checkedVals[i]] = {
                startTime,
                endTime
            };
        }

        let form_data = new FormData()
        form_data.append('name', name)
        form_data.append('address', address)
        form_data.append('email', email)
        form_data.append('description', description)                  
        form_data.append('phonenumber', phonenumber)
        form_data.append('slug', slug)
        form_data.append('sched', JSON.stringify(sched))
        form_data.append('image', image)

        $.ajax({
            type: 'POST',
            url: '/api-create-vendor',
            async: false,
            contentType: false,
            cache: false,
            processData:false,
            data: form_data,
            dataType: 'JSON',
            success: function (res) {
                console.log(res)
                alert('Registered Successfully!');

                $('#name').val('');
                $('#address').val('');
                $('#email').val('');
                $('#phonenumber').val('');
                $('#description').val('');
                $('#image').val('');
                updateSched(res.vendor_id, sched)
            },
            error: function (res) {
                console.log(res)
            }
        });
    });

    function updateSched(id, sched) {
          $.ajax({
            type: 'POST',
            url: '/api-update-vendor-sched',
            async: false,
            data: {
                id: id,
                sched: sched
            },
            dataType: 'JSON',
            success: function (res) {
                alert('Updated Successfully!');
            },
            error: function (res) {
                console.log(res)
            }
        });
    }

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
</script>
