<?php require('layout/header.php'); ?>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="wrapper">
    <?php require('layout/navbar.php'); ?>
    <?php require('layout/sidebar.php'); ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php require('layout/small-navbar.php'); ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0"><?php echo $ticketDetails['name']?></h3>
                        <div class="checkbox">
                            <input id="availability" type="checkbox" <?php echo $ticketDetails['availability'] ? 'checked' : '' ?>>
                            <label for="checkbox0"> Ticket is unavailable </label>
                        </div>
                        <br>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-12">Vendor</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="vendorSelect" disabled>
                                        <?php foreach($vendorList as $key => $value) { ?>
                                            <option 
                                                value="<?php echo $value['id'] ?>"
                                                <?php echo $value['id'] == $ticketDetails['vendor_id'] ? 'selected' : '' ?>
                                                >
                                                <?php echo $value['name'] ?>    
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name" value="<?php echo $ticketDetails['name']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="5" id="description"><?php echo $ticketDetails['description']?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Price</label>
                                <div class="col-md-12">
                                    <input type="number" class="form-control" id="price" step="0.01" value="<?php echo $ticketDetails['price']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control-file" id="image">
                                </div>
                            </div>
                            <?php
                                $image = $ticketDetails['image'] && $ticketDetails['image'] != 'null' ? $ticketDetails['image'] : 'notfound.jpg';
                            ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="card">
                                      <img 
                                        src="public/images/vendor/<?php echo $image?> " 
                                        class="card-img-top" alt="..."
                                        style="width: 100%; object-fit: cover;"
                                        >
                                    </div>    
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-info update-ticket-btn">submit</button>
                            <button class="btn btn-danger delete-ticket-btn">delete ticket</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('layout/footer.php'); ?>
<script>
    let availability = <?php echo $ticketDetails['availability'] ? 1 : 0 ?>;
    let ticketId = <?php echo $ticketDetails['id']; ?>;
    let vendorId = <?php echo $ticketDetails['vendor_id']; ?>;

    $( "#availability" ).on( "click", function() {
        availability = $(this).prop("checked") ? 1 : 0;
    });

    $('.update-ticket-btn').click(function() {
        let name = $('#name').val();
        let price = $('#price').val();
        let description = $('#description').val();
        let image = null;
        let slug = convertToSlug(name);

        if (!name || !price || !description || !vendorId || !slug) {
            alert('Please enter your data');
            return;
        }
        
        if ($("#image").val()) {
            image = $('#image').prop('files')[0];
        } else {
            image = '<?php echo $ticketDetails['image'] ?>';
        }

        let form_data = new FormData()
        form_data.append('id', ticketId)
        form_data.append('name', name)
        form_data.append('description', description)
        form_data.append('price', price)
        form_data.append('image', image)
        form_data.append('vendorId', vendorId)
        form_data.append('availability', availability)
        form_data.append('slug', slug)

        $.ajax({
            type: 'POST',
            url: '/api-update-ticket',
            async: false,
            contentType: false,
            cache: false,
            processData:false,
            data: form_data,
            dataType: 'JSON',
            success: function (res) {
                alert('Updated Successfully!');
            },
            error: function (res) {
                console.log(res)
            }
        });
    });

    $('.delete-ticket-btn').click(function() {
        $.ajax({
            type: 'POST',
            url: '/api-delete-ticket',
            async: false,
            data: {
                id: vendorId,
            },
            dataType: 'JSON',
            success: function (res) {
                alert('Deleted Successfully!');
                window.location = "/ticket-list";
            },
            error: function (res) {
                console.log(res)
            }
        });
    });

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
</script>
