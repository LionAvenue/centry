<?php require('layout/header.php'); ?>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="wrapper">
    <?php require('layout/navbar.php'); ?>
    <?php require('layout/sidebar.php'); ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php require('layout/small-navbar.php'); ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Create New User</h3>
                        <br>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-12">Firstname</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="firstname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Middlename</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="middlename">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Lastname</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="lastname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Address</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Contact Number</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="mobilenumber">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Sex</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="sex">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Birthday</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control" id="birthday">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Role</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="role">
                                        <option value="3">Super Admin</option>
                                        <option value="2">Admin</option>
                                        <option value="1">User</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Vendor</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="vendorId">
                                        <option value="">None</option>
                                        <?php foreach($vendorList as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-info register-btn">submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('layout/footer.php'); ?>
<script>
 $('.register-btn').click(function() {
        let firstname = $('#firstname').val()
        let lastname = $('#lastname').val()
        let middlename = $('#middlename').val()
        let address = $('#address').val()
        let mobilenumber = $('#mobilenumber').val()
        let sex = $('#sex').val()
        let birthday = $('#birthday').val()
        let email = $('#email').val()
        let password = $('#password').val()
        let role = $('#role').val()
        let vendorId = $('#vendorId').val()

        if (!firstname || !lastname || !middlename || !address || !mobilenumber || !sex || !birthday || !email || !password || !role) {
            alert('Please enter your data');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/api-register',
            data: {
                firstname: firstname,
                lastname: lastname,
                middlename: middlename,
                address: address,
                mobilenumber: mobilenumber,
                sex: sex,
                birthday: birthday,
                email: email,
                password: password,
                role: role,
                vendorId: vendorId,
            },
            success: function (res) {
                alert('Registered Successfully!');
                window.location.replace('/create-user');
            },
            error: function (res) {
                console.log(res)
            }
        });
    });
</script>
