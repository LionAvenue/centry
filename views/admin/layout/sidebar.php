<link href="../../public/client/custom/myStyle.css" rel="stylesheet">
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                   <?php echo $user['firstname']; ?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="/profile" class="logout-btn"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <?php if ($user['role'] == 3) { ?>
                <li>
                    <a href="user.html" class="waves-effect">
                        <i class="mdi mdi-account fa-fw"></i> 
                        <span class="hide-menu">User
                            <span class="fa arrow"></span>
                        </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="/create-user"><i class="ti-plus fa-fw"></i><span class="hide-menu">Create user</span></a></li>
                        <li><a href="/user-list"><i class="ti-list fa-fw"></i><span class="hide-menu">User List</span></a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($user['role'] == 3) { ?>
             <li>
                <a href="vendor.html" class="waves-effect">
                    <i class="mdi mdi-newspaper fa-fw"></i> 
                    <span class="hide-menu">Vendor
                        <span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="/create-vendor"><i class="ti-plus fa-fw"></i><span class="hide-menu">Create Vendor</span></a></li>
                    <li><a href="/vendor-list"><i class="ti-user fa-fw"></i><span class="hide-menu">Vendor List</span></a></li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="ticket.html" class="waves-effect">
                    <i class="mdi mdi-ticket-confirmation fa-fw"></i> 
                    <span class="hide-menu">Ticket
                        <span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="/create-ticket"><i class="ti-plus fa-fw"></i><span class="hide-menu">Create Ticket</span></a></li>
                    <li><a href="/ticket-list"><i class="ti-list fa-fw"></i><span class="hide-menu">Ticket List</span></a></li>
                </ul>
            </li>
            <li>
                <a href="booking.html" class="waves-effect">
                    <i class="mdi mdi-bell-outline fa-fw"></i> 
                    <span class="hide-menu">Booking
                        <span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="/calendar"><i class="ti-calendar fa-fw"></i><span class="hide-menu">Calendar</span></a></li>
                </ul>
            </li>

            <li>
                <a href="/" class="waves-effect">Back to Main Page
             </a>
            </li>

        </ul>
    </div>
</div>