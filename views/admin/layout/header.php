<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="views/admin/plugins/images/favicon.png">
    <title>Centry Admin Dashboard</title>
    <link href="views/admin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="views/admin/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
    <link href="views/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="views/admin/css/animate.css" rel="stylesheet">
    <link href="views/admin/css/style.css" rel="stylesheet">
    <link href="views/admin/css/colors/default.css" id="theme" rel="stylesheet">
    <link href="views/admin/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="../../public/client/custom/myStyle.css" rel="stylesheet">
</head>
<body class="fix-header">