<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <a class="logo" href="/dashboard">
                <span class="hidden-xs text-dark">
                    Centry - Admin
                </span>
            </a>
        </div>
        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    <b class="hidden-xs"><?php echo $user['firstname'] ?> </b><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-text">
                                <h4><?php echo $user['firstname'] ?> </h4>
                            </div>
                        </div>
                    </li>
                    <li><a href="/" class="waves-effect">Back to Main Page</a></li>
                    <li><a href="/profile"  class="logout-btn"><p style ="color:red"><i class="fa fa-power-off"></i> Logout</p></a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>