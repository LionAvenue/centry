<script src="views/admin/plugins//bower_components/jquery/dist/jquery.min.js"></script>
<script src="views/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="views/admin/plugins//bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="views/admin/js/jquery.slimscroll.js"></script>
<script src="views/admin/js/waves.js"></script>
<script src="views/admin/js/custom.min.js"></script>
<script src="views/admin/plugins//bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<script src="views/admin/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
	$('.logout-btn').click(function() {
		$.ajax({
	      	type: 'POST',
	      	url: '/api-logout',
	      	dataType: 'JSON',
		    success: function (data) {
		  		if (data.status == 'ok') {
		  			window.location.replace('/');
		  		} else {
		  			alert(data.message);
		  		}      
		    },
	      	error: function (data) {
	        	console.log(data)
	     	}
	    });
	});
</script>
</body>
</html>