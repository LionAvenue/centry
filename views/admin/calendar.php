<?php require('layout/header.php'); ?>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="wrapper">
        <?php require('layout/navbar.php'); ?>
        <?php require('layout/sidebar.php'); ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <?php require('layout/small-navbar.php'); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
			                <div id="bookingCalendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade event-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <table id="myTable" class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">vendor</th>
                        <th scope="col">ticket</th>
                        <th scope="col">quantity</th>
                        <th scope="col">price</th>
                        <th scope="col">date</th>
                        <th scope="col">time</th>
                        <th scope="col">status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="form-group d-none selectStatusOption">
	    <select class="form-control selectedStatus" id="0">
	      <option value="0" selected="false">pending</option>
	      <option value="1" selected="false">approved</option>
	      <option value="2" selected="false">disapproved</option>
	    </select>
  	</div>
<?php require('layout/footer.php'); ?>
<!-- Calendar JavaScript -->
<script src="views/admin/plugins/bower_components/calendar/jquery-ui.min.js"></script>
<script src="views/admin/plugins/bower_components/moment/moment.js"></script>
<script src='views/admin/plugins/bower_components/calendar/dist/fullcalendar.min.js'></script>
<script src="views/admin/plugins/bower_components/calendar/dist/jquery.fullcalendar.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        let calendartable = $('#myTable').DataTable();

        let reservationList = <?php echo json_encode($reservationList); ?>;
		let reservationDates = reservationList.map(a => a.date_start);

		reservationDates = [...new Set(reservationDates)];

		let reservationEvents = [];

		for (var i = reservationDates.length - 1; i >= 0; i--) {
			let date = reservationDates[i];
			let thisBooking = reservationList.filter(b => b.date_start == date);
			let countBooking = thisBooking.length;

			reservationEvents.push({
				title: `Booking (${countBooking})`,
				start: date
			})
		}

		$('#bookingCalendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultView: 'month',
			editable: true,
			events: reservationEvents,
			eventClick: function(info) {
				let momentDate = moment(info.start)
				let date = momentDate.format('LL');
				let resevartionsOnDate = reservationList.filter(b => b.date_start == momentDate.format('YYYY-MM-DD'));
				
				calendartable.clear().draw();

				for (var i = resevartionsOnDate.length - 1; i >= 0; i--) {
					let reservation = resevartionsOnDate[i];
					let optionStatus = $('.selectStatusOption').clone().removeClass('d-none');

					optionStatus.find('select').attr('id', reservation.id);
					optionStatus.find('option').each(function(a) {
						if ($(this).val() == reservation.is_approved) {
							$(this).attr('selected', 'selected')
						} else {
							$(this).removeAttr('selected');
						}
					});

					let btnDefault = reservation.is_approved ? `<button class="btn btn-danger">Disapproved</button>` : `<button class="btn btn-success">Approved</button>`;

					calendartable.row.add([
			          reservation.vendor.name,
			          reservation.ticket.name,
			          reservation.quantity,
			          reservation.total_price,
			          reservation.date_start,
			          reservation.time_start,
			          optionStatus.html()
			        ]).draw(false);
				}

				$('#exampleModalLabel').text(date);
				$('.event-modal').modal('show');
			}
		});
    });

    $(document).on('change', '.selectedStatus', function() {
		let id = $(this).attr('id');
		let status = $(this).val()

		$.ajax({
	      	type: 'POST',
	      	url: '/api-update-status',
	      	data: {
	      		id, 
	      		status,
	      	},
	      	dataType: 'JSON',
		    success: function (data) {
		  		if (data.status == 'ok') {
		  			alert('Status Updated!');
		  		} else {
		  			alert("Something wen't wrong!");
		  		}      
		    },
	      	error: function (data) {
	        	console.log(data)
	     	}
	    });
	})
</script>
