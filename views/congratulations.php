<?php require('layout/header.php'); ?>

<section class="item content">
	<div class="container toparea">
		<div class="jumbotron jumbotron-fluid text-center">
		  <div class="container">

		    <h1 style="color:#ffa331">Congratulations!</h1>
		    <i><h2 style="color:#00bba7">Booking success!</h2></i>
		  </div>
		</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>