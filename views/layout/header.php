<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="generator" content="">
	<link href="../public/client/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="../public/client/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
	<link href="../public/client/custom/myStyle.css" rel="stylesheet">
	<title>Centry | <?php echo $page?></title>
</head>
<body>

<header class="item header margin-top-0">
	<div class="wrapper">
		<nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg navbar-72 navbar-fixed-top">
		<div class="container">
			<div class="navbar-header_main">
				<button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
				<i class="fa fa-bars"></i>
				<span class="sr-only">Toggle navigation</span>
				</button>
				<a href="/" class="navbar-brand brand"> CENTRY </a>
			</div>
			<div id="navbar-collapse-02" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="propClone"><a href="/">Home</a></li>
					<li class="propClone"><a href="/vendor">Vendors</a></li>
					<!-- <li class="propClone"><a href="/product">Product</a></li> -->
					<!-- <li class="propClone"><a href="/checkout">Summaries</a></li> -->
					<!-- <li class="propClone"><a href="/contact">Contact</a></li> -->

					<?php if($user) { ?>
						<li class="propClone">
							<a href="/profile">
								<?php echo $user['firstname'] ?>
							</a>
						</li>
					<?php } else { ?>
						<li class="propClone"><a href="/login">Login</a></li>
						<li class="propClone"><a href="/register">Register</a></li>
					<?php }?>
				</ul>
			</div>
		</div>
		</nav>

		<?php 
		    if ($page == 'home') {
		    	require('views/layout/big-header.php');
			} else {
				require('views/layout/small-header.php');
			}
		?>
	</div>
</header>