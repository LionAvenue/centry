<section class="content-block" style="background-color:#00bba7;">
<div class="container text-center">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="item" data-scrollreveal="enter top over 0.4s after 0.1s">
				<h1 class="callactiontitle"> USE CODE TO GET 20% OFF!<span class="callactionbutton"><i class="fa fa-gift"></i> ILYPROGRAMMING</span>
				</h1>
			</div>
		</div>
	</div>
</div>
</section>

<div class="footer text-center">
	<div class="container">
		<div class="row">
			<p class="footernote">
				 Connect with our Social Media Accounts!
			</p>
			<ul class="social-iconsfooter">
				<li><a class="phone" href="#"><i class="fa fa-phone"></i></a></li>
				<li><a class ="facebook" href="https://www.facebook.com/enriquez.neil.ian.toribio"><i class="fa fa-facebook"></i></a></li>
				<li><a class ="twitter" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
				<li><a class ="google" href="https://accounts.google.com/signin/v2/identifier?hl=en&passive=true&continue=https%3A%2F%2Fwww.google.com%2F&ec=GAZAmgQ&flowName=GlifWebSignIn&flowEntry=ServiceLogin"><i class="fa fa-google-plus"></i></a></li>
				<li><a class ="pinterest" href="https://www.pinterest.ph/"><i class="fa fa-pinterest"></i></a></li>
			</ul>
			<p>
				 &copy; 2021 Centry<br/>
				Centry - Let's Find your Destination by <a href="https://www.facebook.com/enriquez.neil.ian.toribio">Neil Enriquez</a>
			</p>
		</div>
	</div>
</div>

<!-- SCRIPTS =============================-->
<script src="../public/client/js/jquery-.js"></script>
<script src="../public/client/js/bootstrap.min.js"></script>
<script src="../public/client/js/anim.js"></script>
<script src="../public/client/moment/moment.js"></script>
<script>
//----HOVER CAPTION---//	  
jQuery(document).ready(function ($) {
	$('.fadeshop').hover(
		function(){
			$(this).find('.captionshop').fadeIn(150);
		},
		function(){
			$(this).find('.captionshop').fadeOut(150);
		}
	);
});
</script>
	
</body>
</html>