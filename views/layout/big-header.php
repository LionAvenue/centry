<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="text-homeimage">
				<div class="maintext-image" data-scrollreveal="enter top over 1.5s after 0.1s">
					 Explore the Philippines
				</div>
				<div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.3s">
					 Let's find your destination
				</div>
			</div>
		</div>
	</div>
</div>