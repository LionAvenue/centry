<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="text-pageheader">
				<div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.0s">
					<?php echo ucfirst($page); ?>
				</div>
			</div>
		</div>
	</div>
</div>