<?php require('layout/header.php'); ?>
<section class="item content">
	<div class="container toparea">
		<div id="edd_checkout_wrap" class="col-md-8 col-md-offset-2">
			<form>
			  	<div class="form-group">
			    	<label for="exampleInputEmail1">Email address</label>
			    	<input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
			  	</div>
			  	<div class="form-group">
			    	<label for="exampleInputPassword1">Password</label>
			    	<input type="password" class="form-control" id="password" placeholder="Password">
			  	</div>
			  	<hr>
			  	<button type="button" class="btn btn-primary login-btn">Submit</button>
			</form>
		</div>
	</div>
</section>

<?php require('layout/footer.php'); ?>

<script type="text/javascript">
	$('.login-btn').click(function() {
		let email = $('#email').val();
		let password = $('#password').val();

		if (!email || !password) {
			alert('Please enter your data');
			return;
		}

		$.ajax({
	      	type: 'POST',
	      	url: '/api-login',
	      	data: {
	      		email, 
	      		password,
	      	},
	      	dataType: 'JSON',
		    success: function (data) {
		  		if (data.status == 'ok') {
		  			window.location.replace('/');
		  		} else {
		  			alert(data.message);
		  		}      
		    },
	      	error: function (data) {
	        	console.log(data)
	     	}
	    });
	});
</script>
