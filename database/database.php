<?php
require('env.php');

class Database
{
	public function db() {
		$server = $_ENV["server"];
		$username = $_ENV["username"];
		$password = $_ENV["password"];
		$database_name = $_ENV["database"];
		$charset = "UTF8";

		try {
			$dsn = "mysql:host=".$server.";dbname=".$database_name.";dbpassword=".$password.";charset=".$charset;
			$pdo = new PDO($dsn, $username, $password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
}

?>
