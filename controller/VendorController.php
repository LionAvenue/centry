<?php 
require_once('database/database.php');

/**
 * 
 */
class VendorController
{
	public static function createVendor()
	{
		//  Create an object and output the value of one of its properties:
		$database = new database();

		// Connect to the database
		$conn = $database->db();

		// Get post data
		$name = $_POST['name'];
		$description = $_POST['description'];
		$address =$_POST['address'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$slug = $_POST['slug'];
		$image = "";


		// check if data has image and move image to the folder
		if (isset($_FILES['image'])) {		
			$src = $_FILES['image']['tmp_name'];
			$imagePath = "public/images/vendor/".$_FILES['image']['name'];
			move_uploaded_file($src, $imagePath);
			$image = $_FILES['image']['name']; 
		}

		// Prepare the SQL Statment
		$execute = $conn->prepare("INSERT INTO `vendor` (`name`, `description`, `address`, `email`, `phonenumber`, `slug`, `image`) VALUES (?, ?, ?, ?, ?, ?, ?)");

		// execute the SQL Statment and with it's values
		$execute->execute([$name, $description, $address, $email, $phonenumber, $slug, $image]);

		// Get last Insert ID
		$vendorId = $conn->lastInsertId();

    	echo json_encode(array('status' => 'ok', 'message' => 'success', 'vendor_id' => $vendorId));
	}

	public static function updateVendor()
	{
		$database = new database();
		$conn = $database->db();

		$vendorId = $_POST['id'];
		$vendor = VendorController::vendorDetails($vendorId);

		$name = $_POST['name'];
		$description = $_POST['description'];
		$address =$_POST['address'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$slug = $_POST['slug'];
		$image = "";

		if (isset($_FILES['image'])) {		
			$src = $_FILES['image']['tmp_name'];
			$imagePath = "public/images/vendor/".$_FILES['image']['name'];
			move_uploaded_file($src, $imagePath);
			$image = $_FILES['image']['name']; 
		} else {
			$image = $_POST['image'];
		}

		$execute = $conn->prepare("UPDATE `vendor` set `name` = ?, `description` = ?, `address` = ?, `email` = ?, `phonenumber` = ?, `slug` = ?, `image` = ? WHERE  `id` = ?");

		$execute->execute([$name, $description, $address, $email, $phonenumber, $slug, $image, $vendorId]);

		echo json_encode(array('status' => 'OK', 'message' => 'Vendor Schedule deleted!'));
	}

	public static function createVendorSched()
	{
		$id = $_POST['id'];
		$vendorSchedule = VendorController::viewVendorSched($id);

		if ($vendorSchedule) {
			VendorController::deleteVendorSched($id);
		}

		$database = new database();
		$conn = $database->db();
		$sched =  $_POST['sched'];

		$execute = $conn->prepare("INSERT INTO `vendor_schedule` (`day`, `start_time`, `end_time`, `vendor_id`) VALUES (?, ?, ?, ?)");

		foreach ($sched as $key => $value) {
			$day = $key;

			if (count($value)) {
				$execute->execute([$day, $value['startTime'], $value['endTime'], $id]);
			}
		}

		return $vendorSchedule = VendorController::viewVendorSched($id);
	}

	public static function viewVendorSched($id) 
	{
		$vendorId = $id;

    	$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `vendor_schedule`  WHERE `vendor_id` = ?");
    	$stmt->execute([$vendorId]);
    	$rows = $stmt->fetchAll();
    	
		return $rows;
	}

	public static function deleteVendorSched($id) 
	{
		$database = new database();
		$conn = $database->db();
		$vendorId = $id;
       
	    $prepare = $conn->prepare("DELETE FROM `vendor_schedule` WHERE vendor_id = ?");
	  	$prepare->execute([$id]);   

		return json_encode(array('status' => 'OK', 'message' => 'Vendor Schedule deleted!'));
	}

	public static function vendorList()
	{
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `vendor`");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();
    	
		return $rows;
	}

	public static function vendorDetails($id)
	{
		$conn = new database();

    	$stmt = $conn->db()->prepare("SELECT * FROM `vendor` WHERE `id` = ?");
    	$stmt->execute([$id]);
    	$row = $stmt->fetch();

    	$vendorSched = new VendorController();
    	$vendorSched = VendorController::viewVendorSched($row['id']);

    	if (count($vendorSched)) {
    		return array_merge($row, [
    			'vendor_schedule' => $vendorSched
    		]);
    	}

    	return $row;
	}

	public static function vendorDetailsBySlug($slug)
	{
		$conn = new database();

    	$stmt = $conn->db()->prepare("SELECT * FROM `vendor` WHERE `slug` = ?");
    	$stmt->execute([$slug]);
    	$vendor = $stmt->fetch();

    	$vendorSched = new VendorController();
    	$vendorSched = VendorController::viewVendorSched($vendor['id']);

    	if (count($vendorSched)) {
    		return array_merge($vendor, [
    			'vendor_schedule' => $vendorSched
    		]);
    	}

    	return $vendor;
	}
}
?>