<?php 
require_once('database/database.php');

/**
 * 
 */
class UserController
{
	public static function login()
	{
		$conn = new database();

		$email = $_POST['email'];
		$password = md5($_POST['password']);

    	$connect = $conn->db()->prepare("SELECT * FROM `user` WHERE `email` = ? AND `password` = ?");
    	$connect->execute([$email, $password]);
    	$row = $connect->fetch();

    	if (empty($row)) {
			echo json_encode(array('status' => 'error', 'message' => 'Invalid email or password!')); 
			return;
    	}

    	$_SESSION['user'] = $row;

    	echo json_encode(array('status' => 'ok', 'message' => 'success'));
	}

	public static function register()
	{
		$conn = new database();

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$middlename = $_POST['middlename'];
		$address = $_POST['address'];
		$mobilenumber = $_POST['mobilenumber'];
		$sex = $_POST['sex'];
		$birthday = $_POST['birthday'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$role = $_POST['role'];
		$vendorId = $_POST['vendorId'] ? $_POST['vendorId'] : null;

		$password = md5($password);

		$connect = $conn->db()->prepare("INSERT INTO `user` (`firstname`, `lastname`, `middlename`, `address`, `mobilenumber`, `sex`, `birthday`,`email`, `password`, `role`, `vendor_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		$connect->execute([$firstname, $lastname, $middlename, $address, $mobilenumber, $sex, $birthday, $email, $password, $role, $vendorId]);

    	echo json_encode(array('status' => 'ok', 'message' => 'success'));
	}

	public static function logout()
	{
		session_destroy();

		echo json_encode(array('status' => 'ok', 'message' => 'success'));
	}

	public static function userList()
	{
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `user`");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();
    	
		return $rows;
	}

	public static function userDetails($id)
	{
		$conn = new database();
    	$stmt = $conn->db()->prepare("SELECT * FROM `user` WHERE `id` = ?");
    	$stmt->execute([$id]);
    	$row = $stmt->fetch();

    	return $row;
	}

	public static function update()
	{
		$database = new database();
		$conn = $database->db();

		$id = $_POST['id'];

		$userDetails = UserController::userDetails($id);

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$middlename = $_POST['middlename'];
		$address = $_POST['address'];
		$mobilenumber = $_POST['mobilenumber'];
		$sex = $_POST['sex'];
		$birthday = $_POST['birthday'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$role = $_POST['role'];
		$vendorId = $_POST['vendorId'] ? $_POST['vendorId'] : null;

		if ($password) {
			$password = md5($password);
		} else {
			$password = $userDetails['password'];
		}

		$execute = $conn->prepare("UPDATE `user` set `firstname` = ?, `lastname` = ?, `middlename` = ?, `address` = ?, `mobilenumber` = ?, `sex` = ?, `birthday` = ?,`email` = ?, `password` = ?, `role` = ?, `vendor_id` = ?  WHERE  `id` = ?");

		$execute->execute([$firstname, $lastname, $middlename, $address, $mobilenumber, $sex, $birthday, $email, $password, $role, $vendorId, $id]);
		
		// $_SESSION['user'] = UserController::userDetails($id);

		echo json_encode(array('status' => 'OK', 'message' => 'Updated Successfully!'));
	}
}
?>