<?php 
require_once('database/database.php');

/**
 * 
 */
class ReservationController
{
	public static function createReservation()
	{
		$conn = new database();

		$vendor_id = $_POST['vendor_id'];
		$ticket_id = $_POST['ticket_id'];
		$user_id = $_POST['user_id'];
		$quantity = $_POST['quantity'];
		$totalPrice = $_POST['totalPrice'];
		$date_start = $_POST['dateStart'];
		$time_start = $_POST['timeStart'];
		$date_created = $_POST['today'];


		$connect = $conn->db()->prepare("INSERT INTO `reservation` (`vendor_id`,`ticket_id`,`user_id`,`quantity`,`total_price`,`date_start`, `time_start`,`date_created`) VALUES (?,?,?,?,?,?,?,?)");

		$connect->execute([$vendor_id, $ticket_id, $user_id, $quantity, $totalPrice, $date_start, $time_start, $date_created]);

    	echo json_encode(array('status' => 'ok', 'message' => 'success'));
	}

	public static function getUserReservationList($user)
	{
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `reservation` WHERE `user_id` = ?");
    	$stmt->execute([$user['id']]);
    	$rows = $stmt->fetchAll();
    	
    	foreach ($rows as $key => $value) {
    		$rows[$key]['vendor'] = VendorController::vendorDetails($value['vendor_id']);
    		$rows[$key]['ticket'] = TicketController::ticketDetails($value['ticket_id']);
    		$rows[$key]['user'] = UserController::userDetails($value['user_id']);
    	}

		return $rows;
	}

	public static function getAllReservationList($vendorId = null)
	{
		$conn = new database();
		$stmt = null;

		if ($vendorId) {
			$stmt = $conn->db()->prepare("SELECT * FROM `reservation` WHERE `vendor_id` = ?");
			$stmt->execute([$vendorId]);
		} else {
			$stmt = $conn->db()->prepare("SELECT * FROM `reservation`");
			$stmt->execute();
		}

    	$rows = $stmt->fetchAll();
    	
    	foreach ($rows as $key => $value) {
    		$rows[$key]['vendor'] = VendorController::vendorDetails($value['vendor_id']);
    		$rows[$key]['ticket'] = TicketController::ticketDetails($value['ticket_id']);
    		$rows[$key]['user'] = UserController::userDetails($value['user_id']);
    	}

		return $rows;
	}

	public static function updateStatus()
	{
		$database = new database();
		$conn = $database->db();

		$id = $_POST['id'];
		$status = $_POST['status'];

		$execute = $conn->prepare("UPDATE `reservation` set `is_approved` = ? WHERE  `id` = ?");

		$execute->execute([$status, $id]);
		
		echo json_encode(array('status' => 'ok', 'message' => 'Updated Successfully!'));
	}
}
?>