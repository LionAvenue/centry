<?php 
/**
 * 
 */
class PageController
{
	// USER PAGES START

	public static function home()
	{
		$page = 'home';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/home.php');
	}

	public static function shop($request)
	{
		$vendorSlug = $request['vendor'];
		$vendorTicketList = TicketController::getVendorTicketListBySlug($vendorSlug);
		$page = 'shop';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/shop.php');
	}

	public static function vendor()
	{
		$page = 'vendor';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
		$vendorList = VendorController::vendorList(); 
		include('views/vendor.php');
	}

	public static function product($request)
	{
		$vendorSlug = $request['vendor'];
		$ticketSlug = $request['product'];

		$vendor = VendorController::vendorDetailsBySlug($vendorSlug);
		$vendorTicket = TicketController::getVendorTicketBySlug($vendorSlug, $ticketSlug);

		$page = 'product';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/product.php');
	}

	public static function contact()
	{
		$page = 'contact';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/contact.php');
	}

	public static function checkout($request)
	{
		$vendorSlug = $request['vendor'];
		$ticketSlug = $request['product'];

		$vendor = VendorController::vendorDetailsBySlug($vendorSlug);
		$vendorTicket = TicketController::getVendorTicketBySlug($vendorSlug, $ticketSlug);

		$page = 'checkout';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/checkout.php');
	}

	public static function login()
	{
		$page = 'login';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/login.php');
	}

	public static function register()
	{
		$page = 'register';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/register.php');
	}

	public static function profile()
	{
		$page = 'profile';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
		$reservationList = ReservationController::getUserReservationList($user);
		include('views/profile.php');
	}

	public static function congratulations()
	{
		$page = 'congratulations';
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		include('views/congratulations.php');
	}

	// USER PAGES END

	// ADMIN PAGES START 

	public static function dashboard()
	{
		$pageService = new PageController;
		$pageService->isAdmin();
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 

		$page = 'dashboard';
		include('views/admin/dashboard.php');
	}

	public static function createVendor()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'create vendor';
		include('views/admin/createVendor.php');
	}

	public static function vendorList()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'vendor list';
		$vendorList = VendorController::vendorList();
		include('views/admin/vendorList.php');
	}

	public static function viewVendor($request)
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'view vendor';
		$id = $request['id'];
		$vendorDetails = VendorController::vendorDetails($id);
		
		include('views/admin/viewVendor.php');
	}

	public static function createticket()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'create ticket';
		$vendorList = VendorController::vendorList();
		include('views/admin/createticket.php');
	}

	public static function ticketList()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'ticket list';
		$ticketList = TicketController::ticketList($user['vendor_id']);
		$vendorList = VendorController::vendorList();
		include('views/admin/ticketList.php');
	}

	public static function viewticket($request)
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$vendorList = VendorController::vendorList();
		$page = 'view ticket';
		$id = $request['id'];
		$ticketDetails = TicketController::ticketDetails($id);
		
		include('views/admin/viewTicket.php');
	}

	public static function createUser()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$vendorList = VendorController::vendorList();
		$page = 'create user';
		include('views/admin/createUser.php');
	}

	public static function userList()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'user list';
		$userList = UserController::userList();
		include('views/admin/userList.php');
	}

	public static function viewUser($request)
	{
		$id = $request['id'];
		$userDetails = UserController::userDetails($id);
		$vendorList = VendorController::vendorList();

		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'view list';
		include('views/admin/viewUser.php');
	}

	public static function viewCalendar()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
		$reservationList = ReservationController::getAllReservationList($user['vendor_id']);
		$page = 'view calendar';
		include('views/admin/calendar.php');
	}

	public static function bookingList()
	{
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		$page = 'view calendar';
		include('views/admin/bookingList.php');
	}


	// ADMIN PAGES END

	private function isAdmin() {
		$user = isset($_SESSION['user']) ? $_SESSION['user'] : null; 
		if(!$user) {
			header("Location: /login");
		}
	}
}
?>