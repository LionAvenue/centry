<?php 
require_once('database/database.php');

/**
 * 
 */
class TicketController
{
	public static function createticket()
	{
		//  Create an object and output the value of one of its properties:
		$database = new database();

		// Connect to the database
		$conn = $database->db();

		// Get post data
		$name = $_POST['name'];
		$description = $_POST['description'];
		$price = $_POST['price'];
		$vendorId = $_POST['vendorId'];
		$slug = $_POST['slug'];
		$image = "";

		if (isset($_FILES['image'])) {		
			$src = $_FILES['image']['tmp_name'];
			$imagePath = "public/images/vendor/".$_FILES['image']['name'];
			move_uploaded_file($src, $imagePath);
			$image = $_FILES['image']['name']; 
		} else {
			$image = $_POST['image'];
		}

		// Prepare the SQL Statment
		$execute = $conn->prepare("INSERT INTO `ticket` (`name`, `description`, `price`, `availability`, `vendor_id`, `image`, `slug`) VALUES (?, ?, ?, ?, ?, ?, ?)");

		// execute the SQL Statment and with it's values
		$execute->execute([$name, $description, $price, 0, $vendorId, $image, $slug]);

		// return message
    	echo json_encode(array('status' => 'ok', 'message' => 'success'));
	}

	public static function deleteticket() 
	{
		$ticketId = $_POST['id'];
		$database = new database();
		$conn = $database->db();
       
	    $prepare = $conn->prepare("DELETE FROM `ticket` WHERE id = ?");
	  	$prepare->execute([$ticketId]);   

		echo json_encode(array('status' => 'OK', 'message' => 'ticket deleted successfully!'));
	}

	public static function ticketList($id = null)
	{
		$conn = new database();

		if ($id) {
			$stmt = $conn->db()->prepare("SELECT * FROM `ticket` WHERE `vendor_id` = ?");
	    	$stmt->execute([$id]);
	    	$rows = $stmt->fetchAll();

			return $rows;
		}

		$stmt = $conn->db()->prepare("SELECT * FROM `ticket`");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();
    	
		return $rows;
	}

	public static function ticketDetails($id)
	{
		$conn = new database();

    	$stmt = $conn->db()->prepare("SELECT * FROM `ticket` WHERE `id` = ?");
    	$stmt->execute([$id]);
    	$row = $stmt->fetch();

    	return $row;
	}

	public static function updateticket()
	{
		$database = new database();
		$conn = $database->db();

		$ticketId = $_POST['id'];
		$name = $_POST['name'];
		$description = $_POST['description'];
		$price =$_POST['price'];
		$availability = $_POST['availability'];
		$vendorId = $_POST['vendorId'];
		$slug = $_POST['slug'];

		$image = "";

		if (isset($_FILES['image'])) {		
			$src = $_FILES['image']['tmp_name'];
			$imagePath = "public/images/vendor/".$_FILES['image']['name'];
			move_uploaded_file($src, $imagePath);
			$image = $_FILES['image']['name']; 
		} else {
			$image = $_POST['image'];
		}

		$execute = $conn->prepare("UPDATE `ticket` set `name` = ?, `description` = ?, `price` = ?, `availability` = ?, `vendor_id` = ?, `image` = ?, `slug` = ? WHERE  `id` = ?");

		$execute->execute([$name, $description, $price, $availability, $vendorId, $image, $slug, $ticketId]);
		
		echo json_encode(array('status' => 'OK', 'message' => 'Updated Successfully!'));
	}

	public static function getVendorTicketListBySlug($slug)
	{
    	$vendor = TicketController::getVendorBySlug($slug);

    	$vendor['tickets'] = TicketController::ticketListWithAvailability($vendor['id']);

    	return $vendor;
	}

	public static function ticketListWithAvailability($id = null)
	{
		$conn = new database();

		if ($id) {
			$stmt = $conn->db()->prepare("SELECT * FROM `ticket` WHERE `vendor_id` = ? AND `availability` = 1");
	    	$stmt->execute([$id]);
	    	$rows = $stmt->fetchAll();

			return $rows;
		}

		$stmt = $conn->db()->prepare("SELECT * FROM `ticket`");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();
    	
		return $rows;
	}

	public static function getVendorBySlug($slug)
	{
		$conn = new database();

    	$stmt = $conn->db()->prepare("SELECT * FROM `vendor` WHERE `slug` = ?");
    	$stmt->execute([$slug]);
    	$vendor = $stmt->fetch();

    	return $vendor;

	}

	public static function getVendorTicketBySlug($vendorSlug, $ticketSlug)
	{
		$vendor = TicketController::getVendorBySlug($vendorSlug);
		$conn = new database();

    	$stmt = $conn->db()->prepare("SELECT * FROM `ticket` WHERE `slug` = ? AND `vendor_id` = ?");
    	$stmt->execute([$ticketSlug, $vendor['id']]);
    	$row = $stmt->fetch();

    	return $row;
	}
}
?>