<?php 

class Route
{
	private $_uri = array();
	private $_method = array();

	public function add($uri, $method = null) 
	{	
		$routeParams = array_filter(explode('/', $uri));

		if (count($routeParams) >= 2) {
			$uri = '/'.array_shift($routeParams);
		}

		$this->_uri[] = $uri == '/' ? '/' : trim($uri, '/');

		if ($method != null) {
			$this->_method[] = $method;
		}
	}

	public function submit() 
	{
		$uriGetParam = isset($_GET['uri']) ? $_GET['uri'] : '/';

		$routeParams = [];

		if ($uriGetParam != '/') {
			$routeParams = explode('/', $uriGetParam);
		} else {
			$uriGetParam = '/';
		}

		if (count($routeParams) >= 2) {
			$uriGetParam = array_shift($routeParams);
		}

		$found = false;

		// var_dump($this->_uri);
		foreach ($this->_uri as $key => $value) 
		{
			if (preg_match("#^$value$#", $uriGetParam)) 
			{
				$classMethod = explode('@', $this->_method[$key]);

				if (count($classMethod) < 2) {
					echo "Method Not Found!";
					return;
				}

				$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

				$url_components = parse_url($url);

				$params = [];

				if (isset($url_components['query'])) {
					parse_str($url_components['query'], $params);
				}

				$useMethod = $classMethod[0];
				$Method = $classMethod[1];

				new $useMethod();

				call_user_func_array(array($useMethod, $Method), [$params]);

				$found = true;
				break;
			}
		}

		if (!$found) {
			echo "<h1>404 PAGE NOT FOUND!</h1>";
		}
	}
}

?>