<?php
	session_start();

	include('route/route.php');

	include('controller/PageController.php');
	include('controller/UserController.php');
	include('controller/VendorController.php');
	include('controller/TicketController.php');
	include('controller/ReservationController.php');

	$route = new Route();

	// PAGES START CLIENT
	$route->add('/', 'PageController@home');
	$route->add('/vendor', 'PageController@vendor');
	$route->add('/shop', 'PageController@shop');
	$route->add('/product', 'PageController@product');
	$route->add('/checkout', 'PageController@checkout');
	$route->add('/contact', 'PageController@contact');
	$route->add('/login', 'PageController@login');
	$route->add('/register', 'PageController@register');
	$route->add('/profile', 'PageController@profile');
	$route->add('/congratulations', 'PageController@congratulations');
	// PAGES END CLIENT

	// PAGES START ADMIN
	$route->add('/dashboard', 'PageController@dashboard');
	$route->add('/create-vendor', 'PageController@createVendor');
	$route->add('/vendor-list', 'PageController@vendorList');
	$route->add('/view-vendor', 'PageController@viewVendor');

	$route->add('/create-ticket', 'PageController@createTicket');
	$route->add('/ticket-list', 'PageController@ticketList');
	$route->add('/view-ticket', 'PageController@viewTicket');


	$route->add('/create-user', 'PageController@createuser');
	$route->add('/user-list', 'PageController@userList');
	$route->add('/view-user', 'PageController@viewuser');

	$route->add('/calendar', 'PageController@viewCalendar');
	$route->add('/booking-list', 'PageController@bookingList');
	// PAGES END ADMIN

	// API START
	$route->add('/api-login', 'UserController@login');
	$route->add('/api-register', 'UserController@register');
	$route->add('/api-update', 'UserController@update');
	$route->add('/api-logout', 'UserController@logout');
	
	$route->add('/api-create-vendor', 'VendorController@createVendor');
	$route->add('/api-update-vendor', 'VendorController@updateVendor');
	$route->add('/api-delete-vendor', 'VendorController@deleteVendor');
	$route->add('/api-update-vendor-sched', 'VendorController@createVendorSched');

	$route->add('/api-create-ticket', 'TicketController@createticket');
	$route->add('/api-update-ticket', 'TicketController@updateticket');
	$route->add('/api-delete-ticket', 'TicketController@deleteticket');

	$route->add('/api-reservation', 'ReservationController@createReservation');
	$route->add('/api-update-status', 'ReservationController@updateStatus');
	// API END

	$route->submit();
?>
